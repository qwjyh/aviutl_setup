# AviUtlと基本的なプラグイン(拡張編集/入出力)のインストールを行うスクリプト

Write-Output "AviUtlと基本的なプラグイン(拡張編集/入出力)のインストールを行います."
Read-Host "スタートメニューへの追加を行う場合は管理者権限で実行してください.(Enterで先に進む/Ctrl+Cで作業を中止する)"

# check requirements
if(!(Get-Command 7z -ErrorAction SilentlyContinue)) {
  Write-Output "7zip does not exists!"
  Write-Output "Please install 7zip."
  exit 1
}

# introduction
Write-Output "このスクリプトではhttps://aviutl.info/intro/ に基づいてAviUtlと基本的な入出力プラグインのインストールを行います."
Write-Output "インストールは`$install_locationに行います. デフォルトでは`$env:LocalAppData\Programs\AviUtl\です. (Program Filesにしないのは管理者権限の関係の問題です.)"
Read-Host "バージョンが古い可能性があるので各自確認してください."

# basic
# downloading
$install_location = Join-Path $env:LOCALAPPDATA "Programs" "AviUtl"
New-Item -ItemType Directory $install_location -Force
New-Item -ItemType Directory (Join-Path $install_location ".\temp")
Invoke-WebRequest -Uri "http://spring-fragrance.mints.ne.jp/aviutl/aviutl110.zip" -OutFile (Join-Path $install_location "temp" "aviutl110.zip")
Invoke-WebRequest -Uri "http://spring-fragrance.mints.ne.jp/aviutl/exedit93rc1.zip" -OutFile (Join-Path $install_location "temp" "exedit93rc1.zip")

# extracting
Set-Location -Path $install_location
7z.exe x .\temp\aviutl110.zip
7z.exe x .\temp\exedit93rc1.zip

# input plugin
# downloading
Invoke-WebRequest -Uri "https://pop.4-bit.jp/bin/l-smash/L-SMASH_Works_r940_plugins.zip" -OutFile (Join-Path $install_location "temp" "L-SMASH_Works_plugins.zip")

# extracting
New-Item -ItemType Directory "Plugins"
7z.exe e -oPlugins\ .\temp\L-SMASH_Works_plugins.zip lwcolor.auc lwdumper.auf lwinput.aui lwmuxer.auf
Write-Output "AviUtlを起動し、AviUtlの[ファイル]>[環境設定]>[入力プラグインの優先度の設定]で'L-SMASH Work File Reader'や'DirectShow File Reader'の優先度を一番下にしてください。"
.\aviutl.exe
while ($true) {
  $input = Read-Host "終了したらAviUtlを閉じ、'Y'と入力してください"
  if (($input -eq 'Y') -or ($input -eq 'y')) {
    break
  }  
}

# output plugin
# かんたんmp4
Invoke-WebRequest -Uri "https://aoytsk.blog.jp/aviutl/easymp4v0.1.1.zip" -OutFile (Join-Path $install_location "temp" "easymv4.zip")
7z.exe e -oPlugins\ .\temp\easymv4.zip easymp4.auo
# x264guiEx
while ($true) {
  $input = Read-Host "x264guiExをインストールしますか? (Y/N)"
  if ($input -eq 'Y') {
    Invoke-WebRequest -Uri "https://github.com/rigaya/x264guiEx/releases/download/3.16/x264guiEx_3.16.zip" -OutFile (Join-Path $install_location "temp" "x264guiEx.zip")
    7z.exe x -otemp\x264guiEx\ .\temp\x264guiEx.zip
    Move-Item .\temp\x264guiEx\* .
    .\aviutl.exe
    while ($true) {
      $input = Read-Host "AviUtlの[その他]>[出力プラグイン情報]でx264Exが存在することを確かめてください(Y)"
      if ($input -eq 'Y') {
        break
      }
    }
    break
  } elseif ($input -eq 'N') {
    break
  }
}

# create shortcut to add aviutl to Start Menu
# requires administration
while ($true) {
  $input = Read-Host "AviUtlをスタートメニューに追加しますか(管理者権限が必要です)？(Y/N)"
  if ($input -eq 'N') {
    break
  } elseif ($input -eq 'Y') {
    # check administration role
    $currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
    $bool_admin = $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
    if (!$bool_admin) {
      Write-Warning -Message "require Admin privilage
      please run as Administrator"
      exit 1
    }

    # add to start menu
    $startmenu_path = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\"
    $link_path = Join-Path $install_location "AviUtl.lnk"
    $WshShell = New-Object -ComObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut($link_path)
    $Shortcut.TargetPath = Join-Path $install_location "aviutl.exe"
    $Shortcut.IconLocation = Join-Path $install_location "aviutl.exe"
    $Shortcut.WorkingDirectory = $install_location
    $Shortcut.Save()
    Move-Item AviUtl.lnk $startmenu_path

    break
  }
}

# ending
Write-Output "基本的なインストールが終わりました."
Write-Output "つぎは https://aviutl.info/intro/ の1.③および1.④を行ってください."
Write-Output "1.③ https://aviutl.info/syokisettei/"
Write-Output "1.④ https://aviutl.info/firutazyunnzyo/"