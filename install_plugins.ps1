# プラグインのインストール
Write-Output "AviUtlのプラグインをインストールします。"

# locations
$aviutl_location = Join-Path $env:LOCALAPPDATA "Programs" "AviUtl"
$temp_path = Join-Path $aviutl_location "temp"
Set-Location $aviutl_location

# seekbar
Invoke-WebRequest -Uri "https://aoytsk.blog.jp/aviutl/seekbarx.zip" -OutFile (Join-Path $temp_path "seekbarx.zip")
7z.exe e -oPlugins\ .\temp\seekbarx.zip seekbarx.auf

# InputPipePlugin
Invoke-WebRequest -Uri "https://github.com/amate/InputPipePlugin/releases/download/v1.9/InputPipePlugin_1.9.zip" -OutFile "./temp/InputPipePlugin_1.9.zip"
7z.exe e -oPlugins\ .\temp\InputPipePlugin_1.9.zip InputPipePlugin\InputPipePlugin.aui
7z.exe e -oPlugins\ .\temp\InputPipePlugin_1.9.zip InputPipePlugin\InputPipeMain.exe
.\aviutl.exe
while ($true) {
  Write-Output "AviUtlの[ファイル]>[環境設定]>[入力プラグイン優先度の設定]からInputPipePluginをL-SMASH Worksより上にしてください."
  $input = Read-Host "設定が終了したらAviUtlを閉じてください.終了したら(Y)"
  if ($input -eq 'Y') {
    break
  }
}

# RAM preview
Invoke-WebRequest -Uri "https://github.com/oov/aviutl_rampreview/releases/download/v0.3rc7/rampreview_v0.3rc7.zip" -OutFile ".\temp\rampreview_v0.3rc7.zip"
7z.exe x "-otemp\rampreview_v0.3rc7" .\temp\rampreview_v0.3rc7.zip
Get-ChildItem .\temp\rampreview_v0.3rc7\ | Where-Object Name -NE "ZRamPreview.txt" | Move-Item -Destination .\

# TODO: AMD版QSV?